﻿using Autofac;
using TaxiDispatcher.Core;
using TaxiDispatcher.Core.Interfaces;
using TaxiDispatcher.Infrastructure.Data;

namespace TaxiDispatcher.Client
{
    public static class AutofacFactory
    {
        internal static IContainer Create()
        {
            var builder = new ContainerBuilder();

            builder.RegisterType<RideService>().As<IRideService>();
            builder.RegisterType<RideAccepter>().As<IRideAccepter>();
            builder.RegisterType<RideOrderer>().As<IRideOrderer>();
            builder.RegisterType<RideFactory>().As<IRideFactory>();
            builder.RegisterType<RideRepository>().As<IRideRepository>();
            builder.RegisterType<VehicleFinder>().As<IVehicleFinder>();
            builder.RegisterType<VehicleRepository>().As<IVehicleRepository>();
            builder.RegisterType<Context>().AsSelf();

            return builder.Build();
        }
    }
}
