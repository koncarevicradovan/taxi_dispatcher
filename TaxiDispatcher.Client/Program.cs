﻿using System;
using Autofac;
using TaxiDispatcher.Core.Entities;
using TaxiDispatcher.Core.Enums;
using TaxiDispatcher.Core.Exceptions;
using TaxiDispatcher.Core.Interfaces;

namespace TaxiDispatcher.Client
{
    class Program
    {
        static void Main(string[] args)
        {
            var container = AutofacFactory.Create();
            using (var scope = container.BeginLifetimeScope())
            {
                IRideService rideService = scope.Resolve<IRideService>();

                OrderRideWithReport(rideService, 5, 0, RideType.City, new DateTime(2018, 1, 1, 23, 0, 0));
                OrderRideWithReport(rideService, 0, 12, RideType.InterCity, new DateTime(2018, 1, 1, 9, 0, 0));
                OrderRideWithReport(rideService, 5, 0, RideType.City, new DateTime(2018, 1, 1, 11, 0, 0));
                OrderRideWithReport(rideService, 35, 12, RideType.City, new DateTime(2018, 1, 1, 11, 0, 0));

                GetRideReportByDriver(rideService, 2);
            }
        }

        private static void OrderRideWithReport(IRideService rideService, int locationFrom, int locationTo, RideType rideType, DateTime date)
        {
            try
            {
                Console.WriteLine($"Ordering ride from {locationFrom} to {locationTo}...");
                rideService.BookRide(locationFrom, locationTo, rideType, date);
                Console.WriteLine("");
            }
            catch (NoAvailableVehicleException ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        private static void GetRideReportByDriver(IRideService rideService, int driverId)
        {
            Console.WriteLine($"Driver with ID = {driverId} earned today:");

            int total = 0;
            foreach (Ride r in rideService.GetRideList(driverId))
            {
                total += r.GetPrice();
                Console.WriteLine("Price: " + r.GetPrice());
            }

            Console.WriteLine("Total: " + total);

            Console.ReadLine();
        }
    }
}
