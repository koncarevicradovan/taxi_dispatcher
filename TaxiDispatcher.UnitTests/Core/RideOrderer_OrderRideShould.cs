﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TaxiDispatcher.Core.Interfaces;
using Telerik.JustMock;
using TaxiDispatcher.Core;
using TaxiDispatcher.Core.Entities;
using TaxiDispatcher.Core.Enums;

namespace TaxiDispatcher.UnitTests.Core
{
    [TestClass]
    public class RideOrderer_OrderRideShould
    {
        [TestMethod]
        public void FindNearestVehicleAndCreateRide()
        {
            // Arrange
            var vehicleFinder = Mock.Create<IVehicleFinder>();
            var rideFactory = Mock.Create<IRideFactory>();
            var rideOrderer = new RideOrderer(vehicleFinder, rideFactory);
            int locationFrom = 1;
            int locationTo = 10;
            RideType rideType = RideType.InterCity;
            DateTime dateTime = new DateTime(2018, 12, 10, 21, 30, 0);

            // Act
            var ride = rideOrderer.OrderRide(locationFrom, locationTo, rideType, dateTime);

            // Assert
            Mock.Assert(()=> vehicleFinder.FindNearestVehicle(locationFrom), Occurs.Once());
            Mock.Assert(() => rideFactory.CreateRide(locationFrom, locationTo, rideType, Arg.IsAny<Vehicle>(), dateTime), Occurs.Once());
        }
    }
}
