﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using TaxiDispatcher.Core.Interfaces;
using Telerik.JustMock;
using TaxiDispatcher.Core;

namespace TaxiDispatcher.UnitTests.Core
{
    [TestClass]
    public class RideService_GetRideListShould
    {
        [TestMethod]
        public void GetRideList()
        {
            // Arrange
            var rideOrderer = Mock.Create<IRideOrderer>();
            var rideAccepter = Mock.Create<IRideAccepter>();
            var rideRepository = Mock.Create<IRideRepository>();
            var rideService = new RideService(rideOrderer, rideAccepter, rideRepository);
            int driverId = 1;

            // Act
            rideService.GetRideList(driverId);

            // Assert
            Mock.Assert(()=> rideRepository.GetRideListForDriver(driverId), Occurs.Once());
        }
    }
}
