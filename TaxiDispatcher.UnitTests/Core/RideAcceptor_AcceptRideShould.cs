﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TaxiDispatcher.Core.Interfaces;
using Telerik.JustMock;
using TaxiDispatcher.Core;
using TaxiDispatcher.Core.Entities;

namespace TaxiDispatcher.UnitTests.Core
{
    [TestClass]
    public class RideAcceptor_AcceptRideShould
    {
        [TestMethod]
        public void SaveRideAndUpdateVehiclesLocation()
        {
            // Arrange
            var mockRideRepository = Mock.Create<IRideRepository>();
            var mockVehicleRepository = Mock.Create<IVehicleRepository>();
            var rideAcceptor = new RideAccepter(mockRideRepository, mockVehicleRepository);
            var ride = new Ride(5, 0, new Vehicle(1, new Driver(1, "Petar", new Company(1, "Blue taxi", 12)), 6), new DateTime(2018, 1, 1, 23, 0, 0));

            // Act
            rideAcceptor.AcceptRide(ride);

            // Assert
            Mock.Assert(()=>mockRideRepository.SaveRide(ride), Occurs.Once());
            Mock.Assert(() => mockVehicleRepository.UpdateLocationById(ride.Vehicle.Id, ride.To), Occurs.Once());
        }
    }
}
