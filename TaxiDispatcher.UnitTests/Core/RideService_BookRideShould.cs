﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TaxiDispatcher.Core.Interfaces;
using Telerik.JustMock;
using TaxiDispatcher.Core;
using TaxiDispatcher.Core.Entities;
using TaxiDispatcher.Core.Enums;

namespace TaxiDispatcher.UnitTests.Core
{
    [TestClass]
    public class RideService_BookRideShould
    {
        [TestMethod]
        public void OrderRideAndAcceptRide()
        {
            // Arrange
            var rideOrderer = Mock.Create<IRideOrderer>();
            var rideAccepter = Mock.Create<IRideAccepter>();
            var rideRepository = Mock.Create<IRideRepository>();
            var rideService = new RideService(rideOrderer, rideAccepter, rideRepository);
            int locationFrom = 1;
            int locationTo = 10;
            RideType rideType = RideType.InterCity;
            DateTime dateTime = new DateTime(2018, 12, 10, 21, 30, 0);

            // Act
            rideService.BookRide(locationFrom, locationTo, rideType, dateTime);

            // Assert
            Mock.Assert(()=> rideOrderer.OrderRide(locationFrom, locationTo, rideType, dateTime), Occurs.Once());
            Mock.Assert(() => rideAccepter.AcceptRide(Arg.IsAny<Ride>()), Occurs.Once());
        }
    }
}
