﻿using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TaxiDispatcher.Core.Interfaces;
using Telerik.JustMock;
using TaxiDispatcher.Core;
using TaxiDispatcher.Core.Entities;
using TaxiDispatcher.Core.Exceptions;

namespace TaxiDispatcher.UnitTests.Core
{
    [TestClass]
    public class VehicleFinder_FindNearestVehicleShould
    {
        [TestMethod]
        public void GetRideList()
        {
            // Arrange
            var vehicleRepository = Mock.Create<IVehicleRepository>();
            var vehicleFinder = new VehicleFinder(vehicleRepository);
            int locationFrom = 6;
            List<Vehicle> mockVehicles = new List<Vehicle>();
            mockVehicles.Add(new Vehicle(1, new Driver(1, "Sima", new Company(1, "Alo taxi", 20)), 5));
            Mock.Arrange(() => vehicleRepository.GetAll()).Returns(mockVehicles);

            // Act
            vehicleFinder.FindNearestVehicle(locationFrom);

            // Assert
            Mock.Assert(() => vehicleRepository.GetAll(), Occurs.Once());
        }

        [TestMethod]
        [ExpectedException(typeof(NoAvailableVehicleException))]
        public void ThrowNoAvailableVehicleException()
        {
            // Arrange
            var vehicleRepository = Mock.Create<IVehicleRepository>();
            var vehicleFinder = new VehicleFinder(vehicleRepository);
            int locationFrom = 25;
            List<Vehicle> mockVehicles = new List<Vehicle>();
            mockVehicles.Add(new Vehicle(1, new Driver(1, "Sima", new Company(1, "Alo taxi", 20)), 5));
            Mock.Arrange(() => vehicleRepository.GetAll()).Returns(mockVehicles);

            // Act
            vehicleFinder.FindNearestVehicle(locationFrom);
        }
    }
}
