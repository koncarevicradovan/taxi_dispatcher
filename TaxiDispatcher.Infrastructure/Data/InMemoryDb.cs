﻿using System.Collections.Generic;
using TaxiDispatcher.Core.Entities;

namespace TaxiDispatcher.Infrastructure.Data
{
    public class InMemoryDb
    {
        static InMemoryDb()
        {
            SeedData();
        }

        public static List<Ride> Rides { get; } = new List<Ride>();
        public static List<Company> Companies { get; } = new List<Company>();
        public static List<Driver> Drivers { get; } = new List<Driver>();
        public static List<Vehicle> Vehicles { get; } = new List<Vehicle>();

        private static void SeedData()
        {
            Company company1 = new Company(1, "Naxi", 10);
            Company company2 = new Company(2, "Alfa", 15);
            Company company3 = new Company(3, "Gold", 13);

            Companies.Add(company1);
            Companies.Add(company2);
            Companies.Add(company3);

            Driver driver1 = new Driver(1, "Predrag", company1);
            Driver driver2 = new Driver(2, "Nenad", company1);
            Driver driver3 = new Driver(3, "Dragan", company2);
            Driver driver4 = new Driver(4, "Goran", company3);

            Drivers.Add(driver1);
            Drivers.Add(driver2);
            Drivers.Add(driver3);
            Drivers.Add(driver4);


            Vehicle vehicle1 = new Vehicle(1, driver1, 1);
            Vehicle vehicle2 = new Vehicle(2, driver2, 4);
            Vehicle vehicle3 = new Vehicle(3, driver3, 6);
            Vehicle vehicle4 = new Vehicle(4, driver4, 7);

            Vehicles.Add(vehicle1);
            Vehicles.Add(vehicle2);
            Vehicles.Add(vehicle3);
            Vehicles.Add(vehicle4);
        }
    }
}
