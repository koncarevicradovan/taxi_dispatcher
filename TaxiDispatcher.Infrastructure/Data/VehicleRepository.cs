﻿using System.Collections.Generic;
using TaxiDispatcher.Core.Entities;
using TaxiDispatcher.Core.Interfaces;

namespace TaxiDispatcher.Infrastructure.Data
{
    public class VehicleRepository : IVehicleRepository
    {
        private readonly Context _context;

        public VehicleRepository(Context context)
        {
            _context = context;
        }
        
        public List<Vehicle> GetAll()
        {
            return _context.Vehicles.GetAll();
        }

        public Vehicle GetById(int id)
        {
            return _context.Vehicles.GetById(id);
        }

        public void UpdateLocationById(int id, int location)
        {
            var vehicleToUpdate = _context.Vehicles.GetById(id);
            vehicleToUpdate.Location = location;
        }
    }
}
