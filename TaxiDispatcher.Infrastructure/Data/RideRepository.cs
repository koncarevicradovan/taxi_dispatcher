﻿using System.Collections.Generic;
using System.Linq;
using TaxiDispatcher.Core.Entities;
using TaxiDispatcher.Core.Interfaces;

namespace TaxiDispatcher.Infrastructure.Data
{
    public class RideRepository : IRideRepository
    {
        private readonly Context _context;

        public RideRepository(Context context)
        {
            _context = context;
        }

        public void SaveRide(Ride ride)
        {
            _context.Rides.Add(ride);
        }

        public List<Ride> GetRideListForDriver(int driverId)
        {
            return _context.Rides.GetAll(x => x.Vehicle.Driver.Id == driverId).ToList();
        }
    }
}
