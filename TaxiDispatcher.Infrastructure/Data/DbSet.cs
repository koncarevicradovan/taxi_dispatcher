﻿using System;
using System.Collections.Generic;
using System.Linq;
using TaxiDispatcher.Core.Entities;

namespace TaxiDispatcher.Infrastructure.Data
{
    public class DbSet<T> where T : IDomainEntity
    {
        public DbSet(List<T> items)
        {
            Items = items;
        }

        private List<T> _items;

        private List<T> Items
        {
            get
            {
                return _items;
            }
            set
            {
                _items = value;
            }
        }

        public T GetById(int id)
        {
            return Items.FirstOrDefault(x => x.Id == id);
        }

        public List<T> GetAll()
        {
            return Items.ToList();
        }

        public List<T> GetAll(Func<T, bool> condition)
        {
            return Items.Where(condition).ToList();
        }

        public void Add(T entity)
        {
            entity.Id = GetNextIndex();
            Items.Add(entity);
        }

        private int GetNextIndex()
        {
            if (Items.Count == 0)
            {
                return 1;
            }
            return Items.Max(x => x.Id) + 1;
        }
    }
}
