﻿using TaxiDispatcher.Core.Entities;

namespace TaxiDispatcher.Infrastructure.Data
{
    public class Context
    {
        public DbSet<Driver> Drivers { get; }
        public DbSet<Company> Companies { get; }
        public DbSet<Vehicle> Vehicles { get; }
        public DbSet<Ride> Rides { get; }

        public Context()
        {
            Drivers = new DbSet<Driver>(InMemoryDb.Drivers);
            Companies = new DbSet<Company>(InMemoryDb.Companies);
            Vehicles = new DbSet<Vehicle>(InMemoryDb.Vehicles);
            Rides = new DbSet<Ride>(InMemoryDb.Rides);
        }
    }
}
