﻿namespace TaxiDispatcher.Core.Entities
{
    public class Company : IDomainEntity
    {
        public Company(int id, string name, int pricePerKm)
        {
            Id = id;
            Name = name;
            PricePerKm = pricePerKm;
        }
        public int Id { get; set; }
        public string Name { get; }
        public int PricePerKm { get; }
    }
}