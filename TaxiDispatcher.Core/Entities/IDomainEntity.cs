﻿namespace TaxiDispatcher.Core.Entities
{
    public interface IDomainEntity
    {
        int Id { get; set; }
    }
}
