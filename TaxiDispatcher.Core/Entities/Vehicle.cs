﻿namespace TaxiDispatcher.Core.Entities
{
    public class Vehicle : IDomainEntity
    {
        public Vehicle(int id, Driver driver, int location)
        {
            Id = id;
            Driver = driver;
            Location = location;
        }
        public int Id { get; set; }
        public Driver Driver { get; }
        public int Location { get; set; }
    }
}