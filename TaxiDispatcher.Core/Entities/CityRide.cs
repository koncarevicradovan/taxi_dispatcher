﻿using System;

namespace TaxiDispatcher.Core.Entities
{
    public class CityRide : Ride
    {
        public CityRide(int from, int to, Vehicle vehicle, DateTime time)
            : base(from, to, vehicle, time)
        {
        }
    }
}