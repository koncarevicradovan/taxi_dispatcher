﻿using System;

namespace TaxiDispatcher.Core.Entities
{
    public class InterCityRide : Ride
    {
        private const int InterCityRidePounder = 2;

        public InterCityRide(int from, int to, Vehicle vehicle, DateTime time)
            : base(from, to, vehicle, time)
        {
        }

        public override int GetPrice()
        {
            return InterCityRidePounder * base.GetPrice();
        }
    }
}