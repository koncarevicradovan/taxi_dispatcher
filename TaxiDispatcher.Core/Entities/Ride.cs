﻿using System;

namespace TaxiDispatcher.Core.Entities
{
    public class Ride : IDomainEntity
    {
        public Ride(int from, int to, Vehicle vehicle, DateTime time)
        {
            From = from;
            To = to;
            Vehicle = vehicle;
            Time = time;
        }

        public int Id { get; set; }
        public int From { get; }
        public int To { get; }
        public Vehicle Vehicle { get; }
        public DateTime Time { get; }

        public virtual int GetPrice()
        {
            int timePounder = 1;
            if (Time.Hour < 6 || Time.Hour > 22)
            {
                timePounder = 2;
            }
            return Math.Abs(From - To) * Vehicle.Driver.Company.PricePerKm * timePounder;
        }
    }
}