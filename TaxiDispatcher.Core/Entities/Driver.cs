﻿namespace TaxiDispatcher.Core.Entities
{
    public class Driver : IDomainEntity
    {
        public Driver(int id, string name, Company company)
        {
            Id = id;
            Name = name;
            Company = company;
        }
        public int Id { get; set; }
        public string Name { get; }
        public Company Company { get; }
    }
}