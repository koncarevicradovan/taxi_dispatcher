﻿using System;
using TaxiDispatcher.Core.Entities;
using TaxiDispatcher.Core.Interfaces;

namespace TaxiDispatcher.Core
{
    public class RideAccepter : IRideAccepter
    {
        private readonly IRideRepository _rideRepository;
        private readonly IVehicleRepository _vehicleRepository;

        public RideAccepter(IRideRepository rideRepository, IVehicleRepository vehicleRepository)
        {
            _rideRepository = rideRepository;
            _vehicleRepository = vehicleRepository;
        }

        public void AcceptRide(Ride ride)
        {
            _rideRepository.SaveRide(ride);
            _vehicleRepository.UpdateLocationById(ride.Vehicle.Id, ride.To);
            Console.WriteLine("Ride accepted, waiting for driver: " + ride.Vehicle.Driver.Name);
        }
    }
}
