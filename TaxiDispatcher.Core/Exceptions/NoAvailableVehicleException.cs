﻿using System;
using System.Runtime.Serialization;

namespace TaxiDispatcher.Core.Exceptions
{
    public class NoAvailableVehicleException : Exception
    {
        public NoAvailableVehicleException()
        {
        }

        public NoAvailableVehicleException(string message) : base(message)
        {
        }

        public NoAvailableVehicleException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected NoAvailableVehicleException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}
