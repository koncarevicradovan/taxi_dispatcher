﻿using System;
using TaxiDispatcher.Core.Entities;
using TaxiDispatcher.Core.Enums;
using TaxiDispatcher.Core.Interfaces;

namespace TaxiDispatcher.Core
{
    public class RideOrderer : IRideOrderer
    {
        private readonly IVehicleFinder _vehicleFinder;
        private readonly IRideFactory _rideFactory;

        public RideOrderer(IVehicleFinder vehicleFinder, IRideFactory rideFactory)
        {
            _vehicleFinder = vehicleFinder;
            _rideFactory = rideFactory;
        }

        public Ride OrderRide(int locationFrom, int locationTo, RideType rideType, DateTime time)
        {
            Vehicle nearestVehicle = _vehicleFinder.FindNearestVehicle(locationFrom);
            Ride ride = _rideFactory.CreateRide(locationFrom, locationTo, rideType, nearestVehicle, time);
            Console.WriteLine("Ride ordered, price: " + ride.GetPrice());
            return ride;
        }
    }
}
