﻿using System;
using TaxiDispatcher.Core.Entities;
using TaxiDispatcher.Core.Exceptions;
using TaxiDispatcher.Core.Interfaces;

namespace TaxiDispatcher.Core
{
    public class VehicleFinder : IVehicleFinder
    {
        private const int MaxDistance = 15;
        private readonly IVehicleRepository _vehicleRepository;

        public VehicleFinder(IVehicleRepository vehicleRepository)
        {
            _vehicleRepository = vehicleRepository;
        }

        public Vehicle FindNearestVehicle(int locationFrom)
        {
            int nearestVehicleDistance = int.MaxValue;
            Vehicle nearestVehicle = null;
            foreach (Vehicle vehicle in _vehicleRepository.GetAll())
            {
                int vehicleDistance = Math.Abs(vehicle.Location - locationFrom);
                if (vehicleDistance < nearestVehicleDistance)
                {
                    nearestVehicle = vehicle;
                    nearestVehicleDistance = vehicleDistance;
                }
            }

            ThrowIfNoAvailableVehicle(nearestVehicleDistance);

            return nearestVehicle;
        }

        private static void ThrowIfNoAvailableVehicle(int nearestVehicleDistance)
        {
            if (nearestVehicleDistance > MaxDistance)
            {
                throw new NoAvailableVehicleException("There are no available taxi vehicles!");
            }
        }
    }
}
