﻿using System.Collections.Generic;
using TaxiDispatcher.Core.Entities;

namespace TaxiDispatcher.Core.Interfaces
{
    public interface IVehicleRepository
    {
        List<Vehicle> GetAll();
        Vehicle GetById(int id);
        void UpdateLocationById(int id, int location);
    }
}