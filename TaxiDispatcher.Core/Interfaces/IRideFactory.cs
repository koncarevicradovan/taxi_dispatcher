﻿using System;
using TaxiDispatcher.Core.Entities;
using TaxiDispatcher.Core.Enums;

namespace TaxiDispatcher.Core.Interfaces
{
    public interface IRideFactory
    {
        Ride CreateRide(int from, int to, RideType rideType, Vehicle vehicle, DateTime time);
    }
}