﻿using System.Collections.Generic;
using TaxiDispatcher.Core.Entities;

namespace TaxiDispatcher.Core.Interfaces
{
    public interface IRideRepository
    {
        void SaveRide(Ride ride);
        List<Ride> GetRideListForDriver(int driverId);
    }
}