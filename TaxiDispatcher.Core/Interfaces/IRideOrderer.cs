﻿using System;
using TaxiDispatcher.Core.Entities;
using TaxiDispatcher.Core.Enums;

namespace TaxiDispatcher.Core.Interfaces
{
    public interface IRideOrderer
    {
        Ride OrderRide(int locationFrom, int locationTo, RideType rideType, DateTime time);
    }
}