﻿using TaxiDispatcher.Core.Entities;

namespace TaxiDispatcher.Core.Interfaces
{
    public interface IRideAccepter
    {
        void AcceptRide(Ride ride);
    }
}