﻿using System;
using System.Collections.Generic;
using TaxiDispatcher.Core.Entities;
using TaxiDispatcher.Core.Enums;

namespace TaxiDispatcher.Core.Interfaces
{
    public interface IRideService
    {
        void BookRide(int locationFrom, int locationTo, RideType rideType, DateTime date);
        List<Ride> GetRideList(int driverId);
    }
}