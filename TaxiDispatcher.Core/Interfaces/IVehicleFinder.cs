﻿using TaxiDispatcher.Core.Entities;

namespace TaxiDispatcher.Core.Interfaces
{
    public interface IVehicleFinder
    {
        Vehicle FindNearestVehicle(int locationFrom);
    }
}