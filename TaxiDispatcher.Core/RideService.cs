﻿using System;
using System.Collections.Generic;
using TaxiDispatcher.Core.Entities;
using TaxiDispatcher.Core.Enums;
using TaxiDispatcher.Core.Interfaces;

namespace TaxiDispatcher.Core
{
    public class RideService : IRideService
    {
        private readonly IRideRepository _rideRepository;
        private readonly IRideOrderer _rideOrderer;
        private readonly IRideAccepter _rideAccepter;

        public RideService(IRideOrderer rideOrderer, IRideAccepter rideAccepter, IRideRepository rideRepository)
        {
            _rideOrderer = rideOrderer;
            _rideAccepter = rideAccepter;
            _rideRepository = rideRepository;
        }

        public void BookRide(int locationFrom, int locationTo, RideType rideType, DateTime date)
        {
            Ride ride = _rideOrderer.OrderRide(locationFrom, locationTo, rideType, date);
            _rideAccepter.AcceptRide(ride);
        }

        public List<Ride> GetRideList(int driverId)
        {
            return _rideRepository.GetRideListForDriver(driverId);
        }
    }
}
