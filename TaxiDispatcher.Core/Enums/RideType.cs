﻿namespace TaxiDispatcher.Core.Enums
{
    public enum RideType
    {
        City,
        InterCity
    }
}