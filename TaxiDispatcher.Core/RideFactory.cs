﻿using System;
using TaxiDispatcher.Core.Entities;
using TaxiDispatcher.Core.Enums;
using TaxiDispatcher.Core.Interfaces;

namespace TaxiDispatcher.Core
{
    public class RideFactory : IRideFactory
    {
        public Ride CreateRide(int from, int to, RideType rideType, Vehicle vehicle, DateTime time)
        {
            switch (rideType)
            {
                case RideType.InterCity:
                    return new InterCityRide(from, to, vehicle, time);
                case RideType.City:
                    return new CityRide(from, to, vehicle, time);
                default:
                    throw new ArgumentOutOfRangeException(nameof(rideType), rideType, null);
            }
        }
    }
}
